import doctest

def magic(scores, links):
    """
    >>> magic([1,2,3],["NYY","YNN","YNN"])
    4
    
    >>> magic([1,1,1,1],["NNYY","NNYY","YYNN","YYNN"])
    -1
    
    >>> magic([86,15,100,93,53,50],["NYYYYN","YNNNNY","YNNYYN","YNYNYN","YNYYNY","NYNNYN"])
    332
    
    >>> magic([3969,9430,7242,8549,8190,8368,3704,9740,1691],["NYYYYYYYY","YNYYYYYYY","YYNYYYYYY","YYYNYYYYY","YYYYNYYYY","YYYYYNYYY","YYYYYYNNY","YYYYYYNNY","YYYYYYYYN"])
    57179
        
    """
    N = len(scores)
    dictlinks = {}
    for i in range(N):
        dictlinks[i] = []
        for j, s in enumerate(links[i]):
            if (s == 'Y'):
                dictlinks[i].append(j)
    
    #list of all maximal cliques
    l = list(bk(set(), set(dictlinks.keys()), set(), dictlinks))

    all_res = []
    n = len(dictlinks.keys())
    for maximal_clique in l:
        m = len(maximal_clique)
        if n > 1 and 3 * m >= 2 * n:
            res = 0
            for k in maximal_clique:
                res += scores[k]
            all_res.append(res)
    if len(all_res):
        maxmagic = max(all_res)
    else:
        maxmagic = -1
    return maxmagic

def bk(R, P, X, G):
    '''
        iterative bron_kerbosh algorithm 
        generates the maximal cliques list
    '''
    if not P and not X:
        #print "P and X null"
        yield R  #maximum clique

    for v in  P.copy():
        P.remove(v)
        R_v = R.union([v])
        P_v = P.intersection(G[v])
        X_v = X.intersection(G[v])
        for r in bk(R_v, P_v, X_v, G):
            yield r
        X.add(v)


if __name__ == "__main__":
    
    doctest.testmod()
