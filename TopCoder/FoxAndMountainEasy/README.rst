.. _README_fox::

*******************
FoxAndMountainEasy
*******************

Problem
=======

Fox Ciel lives in a beautiful countryside. She loves climbing mountains. Yesterday, she went hiking in the mountains.

Her trip can be described as a sequence of (n+1) integers: h[0], h[1], ..., h[n].These values represent altitudes visited by Fox Ciel during the trip, in order. Fox Ciel does not remember the precise sequence, but she remembers the following:

    for each i, h[i] >= 0
    h[0] = h0
    h[n] = hn
    for each i, abs(h[i+1]-h[i]) = 1


The last condition means that in each step the altitude of Fox Ciel either increased by 1, or decreased by 1. We will call the two types of steps "steps up" and "steps down", respectively. Steps up will be denoted 'U' and steps down will be denoted 'D'.

You are given the ints n, h0, and hn: the length of the trip, the altitude at the beginning, and the altitude at the end. In addition to these, Fox Ciel remembers some contiguous segment of her trip. You are given this segment as a String history. Each character of history is either 'U' or 'D'.

Task
====
Check whether there is a valid trip that matches everything Fox Ciel remembers. 


Input
=====

-	n will be between 1 and 100,000, inclusive.
-	history will contain between 1 and min(50,n) characters, inclusive.
-	Each character in history will be either 'U' or 'D'.
-	h0 will be between 0 and 100,000, inclusive.
-	hn will be between 0 and 100,000, inclusive.

Output
======

Return "YES" (quotes for clarity) if there is at least one such trip, or "NO" if there is none.

Examples
========

    >>> possible(4,0,4,"UU")
    'YES'

    The only solution is: h[] = {0, 1, 2, 3, 4}, the history of the entire trip will be "UUUU".

    >>> possible(4,0,4,"D")
    'NO'

    Based on n, h0 and hn, the history of the entire trip must be "UUUU". There is no 'D' in this history.

    >>> possible(4, 100000, 100000, "DDU")
    'YES'

    We have the following solution: h[] = {100000, 100001, 100000, 99999, 100000}, the history of the entire trip is "UDDU".
    
    >>> possible(4,0,0, "DDU")
    'NO'

    >>> possible(20,20,20,"UDUDUDUDUD")
    'YES'

    >>> possible(20,0,0,"UUUUUUUUUU")
    'YES'

    >>> possible(20,0,0,"UUUUUUUUUUU")
    'NO'

Source
======

http://community.topcoder.com/stat?c=problem_statement&pm=12195&rd=15179



