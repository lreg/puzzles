def possible(n, h0, hn, history):

    h_current = h0
    minh = h0
    for hi in history:
        if hi == 'U':
            h_current += 1
        else:
            h_current -= 1
        minh = min(minh, h_current)
    
    remain_step = n - len(history)
    remain_dist = abs(hn - h_current)
    
    if remain_dist > remain_step:
        return 'NO'
    if not remain_dist % 2 == remain_step % 2:
        return 'NO'
    if minh + remain_step < 0:
        return 'NO'
    
    else:
        return 'YES'
