import itertools
import doctest

def rocks(I, S):
    """
    >>> rocks([9000, 1, 10],[9000, 2, 20])
    15
        
    >>> rocks([1001, 2001, 3001, 3001],[1003, 2003, 3003, 3003])
    9
        
    >>> rocks([9000,90000,1,10],[9000,90000,3,15])
    38
        
    >>> rocks([1,1,1,1,1,1],[3,4,5,6,7,8])
    0
        
    """
    
    nb_box=len(I)
    inter=set()
    #on fait les intervalles generes par toutes les permutations
    #puis on fait l union
    #finalement on prend que les nb sup a 9000
    for n in range(nb_box):
        lcom=list(itertools.combinations(range(nb_box), n+1))
        for l in lcom:
            inf=0
            sup=0
            for i in l:
                inf+=I[i]
                sup+=S[i]
            
            jj=range(inf, sup+1)
            inter=inter.union(jj)

    res=[uu for uu in inter if uu>9000]
    
    return len(res)





if __name__=="__main__":
    doctest.testmod()
