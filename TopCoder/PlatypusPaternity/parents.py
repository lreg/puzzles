def labchild(meres, peres, child):
    """
    >>> labchild(["YYYY", "YYYY"],["NNYN", "YYYN"],["YYYN", "NNNY"] )
    5
    
    >>> labchild(["NNYYY"],["YYNNN"],["YYNNN", "NNYYY"])
    0
    
    >>> labchild(["YYYYYYYYN"],["YYYYYYYYY"],["YNYNYNYNY","NNNYNYNNN","NYNNNNNYN"])
    4
    
    >>> labchild(["YY"],["YY"],["YN", "NY"])
    3
    
    >>> labchild(["YYNNYYNNYYNN","YNYNYNYNYNYN", "YYYNNNYYYNNN"],["NYYNNYYNNYYN","YYNYYYNYYYNY","NNNNNNYYYYYY"],["NYNNNYNNNNNN","NNNNNNNNYNNN","NNYNNNNNNNYN","YNNNNNNYNNNN","NNNNNNNNNYNY","NNNYYNYNNNNN"])
    4
    """
    
    #on peut commencer par ordoner les childs set par ordre de celui qui a le plus de Y
    maxchild=[]
    lg_max=0
    res=0
    for childset in child:
        pos=[]
        for i, c in enumerate(childset):
            if c=='Y':
                pos.append(i)
        #search compatibility
        
        res=findparents(pos, peres, meres)
        lg_max=max(res, lg_max)
    return lg_max


def findparents(groupchild, peres, meres):
    pos=groupchild

    for pere in peres:
        for j, posi in enumerate(pos):
            if not pere[posi]=='Y':
                j-=1
                break

        if j==(len(pos)-1):
            for mere in meres:
                for k, posi in enumerate(pos):
                    if not mere[posi]=='Y':
                        k-=1
                        break
                if k==(len(pos)-1):
#                    print  "PERE", pere, "MERE", mere, "CHILD", pos
                    return len(pos)+2
    return 0



if __name__ == '__main__':
    import doctest
    doctest.testmod()
