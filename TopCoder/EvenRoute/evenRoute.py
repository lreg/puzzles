import doctest
import numpy as np

def isItPossible(X, Y, wantedParity):
    """
    >>> isItPossible([-1,-1,1,1],[-1,1,1,-1],0)
    'CAN'
    
    >>> isItPossible([1001, -4000],[0,0],1)
    'CAN'
    
    >>> isItPossible([-5,-3,2],[2,0,3],1)
    'CAN'
        
    >>> isItPossible([11, 21, 0],[-20, 42, 7],0)
    'CANNOT'
    
    >>> isItPossible([0, 6],[10, -20],1)
    'CANNOT'
    """

    for i in range(0,len(X)):
        if (abs(X[i]+Y[i])%2== wantedParity):
            return 'CAN'
    return 'CANNOT'



if __name__ == "__main__":
    doctest.testmod()