#!/usr/bin/env python

import hadoopy
import pickle 
from random import random
import csv 
import time
import pylab


input_file = 'data.csv'

hdfs_path = 'hadoopy-kmeans/%f/' % time.time()
input_hdfs_file = hdfs_path+"inputdata.csv"
centroids_hdfs_path = hdfs_path+"centroid/"
centroids_hdfs_latest = "hadoopy-kmeans/latest.tb"

script_path = 'kmeans_mr.py'


def parse_data(input_local_file):
    '''
         Parses data of csv file into numpy 2d array 
    '''
    fid = open(input_local_file)
   
    reader = csv.reader(fid)
    reader.next()
    for i,row in enumerate(reader):
        float_row = [float(data) for data in row]
        yield i, float_row
    fid.close()
    
    
def init_centroids(K=10):
    #init centroids randomly
    for i in range(K):
        yield i, [random(), random()]
  
   
def update_centroids(step):
    
    #the previous and current centroids are in the file system
    #we could use only two files
    current_centroids = hadoopy.readtb(centroids_hdfs_path+str(step)+".tb")
    previous_centroids = hadoopy.readtb(centroids_hdfs_path+str(step-1)+".tb")
       
    current_centroids = dict(current_centroids)   
    previous_centroids = dict(previous_centroids) 

    maxdist = 0
    for (k,(x,y)) in current_centroids.items():
        ox,oy = previous_centroids[k]
        dist = (x-ox)**2+(y-oy)**2
        if dist>maxdist:
              maxdist = dist
        
    if maxdist<1e-9:
        return -1
    else:
        return 1
 
 
def plot_data(input_path, centroids_dict):
    import kmeans_mr 
    K = max(centroids_dict.keys())
    mapper = kmeans_mr.Mapper()
    print centroids_dict.keys
    for (id,data) in parse_data(input_path):
        for (cluster, x) in mapper.map(id, data):
            col = cluster*1.0/K
            pylab.plot(data[0],data[1], '.', color = (col,(col*2)%1,(col*3)%1))
    for (cluster, position) in centroids_dict.items():
        pylab.plot(position[0],position[1],'xr')
        
        
def main():
    #init centroids (written on a hdfs file) 
    hadoopy.writetb(centroids_hdfs_path+"0.tb",init_centroids())
    try:
        hadoopy.rmr(centroids_hdfs_latest)
    except:
        pass
    hadoopy.cp(centroids_hdfs_path+"0.tb",centroids_hdfs_latest)
    
    #put data onto the file system
    hadoopy.writetb(input_hdfs_file, parse_data(input_file))
    
    step = 1
    while(True):
        # Note : you cannot rewrite a file  
        fileout = centroids_hdfs_path+str(step)+".tb"
      
        hadoopy.launch(input_hdfs_file, fileout , script_path )
        hadoopy.rmr(centroids_hdfs_latest)
        hadoopy.cp(fileout,centroids_hdfs_latest)
       
        res = update_centroids(step)
        step += 1
        if res==-1:
            print "FINISH"
            break
        
        
if __name__ == "__main__":
    main()
    centroids =  hadoopy.readtb(centroids_hdfs_latest)
    plot_data(input_file, dict(centroids))
    pylab.show() 
    
    
    
    