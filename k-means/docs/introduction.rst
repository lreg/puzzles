.. _intro:

#################################################
Introduction #################################################
 

This document gives a brief view of modern approach for scaling up machine learning methods on parallel/distributed platforms. The need for scaling up machine learning algorithm is task specific: some problems are driven by the size of the data, other problems are driven by the time complexity, etc. 


In this tutorial we propose to analyse a simple clustering algorithm : the K-means. After a :ref:`presentation <kmeans_problem>` of the classical algorithm we propose to solve the problem using the MapReduce paradigm. A first  solution using the MapReduce paradigm to be run on several processors is presented in :ref:`section 2 <kmeans_mapreduce>`. A better solution, using Hadoop is presented in :ref:`section 3 <kmeans_hadoop>`. Finally we review a few tools designed to scale up machine learning algorithm is presented in :ref:`section 4 <kmeans_hadoop>`. 
 

Recent add
===========

The ever increasing size of data and the recent rise in popularity of distributed processing system such as Hadoop fuel the need for ML algorithms that can take advantage of distributed system to tackle the problem of ML with large data set. 


Some ML lend themselves naturally to parallel implementation (bagging and random forest). 
See M. Zinkevich, M. Weimer, A. Smola, and L. Li, "Parallelized Stochastic Gradient Descent," in Advances in Neural Information Processing Systems 23, 2010, pp. 2595-2603.

