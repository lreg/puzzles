.. _source:

###################
Ressources 
###################

Downloadable Books 
===================

`Mining of data set <http://infolab.stanford.edu/~ullman/mmds.html>`_: free e-book focussed on the MapReduce paradigm for large-scale machine Learning 

`Scaling up Machine Learning: Parallel and Distributed Approaches  <http://www.cs.umass.edu/~ronb/scaling_up_machine_learning.htm>`_ by Ron Bekkerman, Misha Bilenko and John Langford.

`Mahout in Action  <http://net.pku.edu.cn/~course/cs402/2012/book/%5BMahout.in.Action%282011%29%5D.Sean.Owen.pdf>`_ by Sean Owen, Robin Anil, Ted Dunning and Ellen Friedman 

`Hadoop The definitive Guide <http://bigdata.googlecode.com/files/Hadoop%20The%20Definitive%20Guide%202nd%20Edition.pdf>`_ by Tom White.
 
`Data-Intensive Text Processing with MapReduce <http://lintool.github.com/MapReduceAlgorithms/>`_ by Jimmy Lin a Chris Dyer of the University of Maryland. This book shows how you can implement a variety of useful algorithms (graph algo such as BFS, Page Rank, HMM…) on a MapReduce cluster.  



Papers
======
The first paper on the subject released by Google in 2003: `The Google File System  <http://research.google.com/archive/gfs.html>`_ 

The next paper, published by Google in 2004: `MapReduce: Simplified Data Processing on Large Clusters <http://static.googleusercontent.com/external_content/untrusted_dlcp/research.google.com/en//archive/mapreduce-osdi04.pdf>`_

Then came in 2006 `Bigtable: A Distributed Storage System for Structured Data <research.google.com/archive/bigtable-osdi06.pdf>`_ which is has given the inspiration for countless NoSQL databases (e.g. Cassandra, HBase…)
 
`MapReduce for machine learning on multicore <http://www.cs.stanford.edu/people/ang//papers/nips06-mapreducemulticore.pdf>`_. This paper gives a very clear explanation to write Locally Weighed Linear Regression, Naive Bayes, Gaussian Discriminative Analysis, k-means, Logistic regression, Neural Network, Principal Component Analysis, Independent Component Analysis, Expectation Minimization and SVM.      

Another paper on the same subject : `MapReduce: Distributed Computing for Machine Learning <http://www1.icsi.berkeley.edu/~arlo/publications/gillick_cs262a_proj.pdf>`_

Several papers on algorithm for large data set are presented every year at the Workshop on Algorithms for Modern Massive Data Sets (MMDS) can be found `here <http://www.stanford.edu/group/mmds/>`_

This `link <http://atbrox.com/2009/10/01/mapreduce-and-hadoop-academic-papers/>`_ gives a nice list of academic papers on the topic of MapReduce. 

Technical Stuff
=================


`Installation and configuration of Hadoop for mac and Linux <http://www.orzota.com/>`_

`Writing python map reduce job to run with Hadoop <http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/>`_


`Nice use of Mahout Recommender System with python <http://chimpler.wordpress.com/2013/02/20/playing-with-the-mahout-recommendation-engine-on-a-hadoop-cluster/>`_ 

Additional Resources
=====================

Other useful information : http://werxltd.com/wp/2009/09/29/using-python-with-hadoop/

http://architects.dzone.com/articles/big-data-beyond-mapreduce

You can find a good list of lecture notes, workshops, publications, software, data for large scale machine learning `here 
<http://www.quora.com/Large-Scale-Learning/What-are-some-introductory-resources-for-learning-about-large-scale-machine-learning-Why#ans104989>`_

Run a simple hadoop job : http://www.thecloudavenue.com/2012/10/debugging-hadoop-mapreduce-program-in.html

Using HDFS in Java: http://blog.rajeevsharma.in/2009/06/using-hdfs-in-java-0200.html
