.. Large Scale k-means documentation master file, created by
   sphinx-quickstart on Tue Mar 19 11:02:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to Large Scale ML : the K-means algorithm
=======================================================

Contents:

.. toctree::
   :maxdepth: 2

   introduction.rst
   kmeans_problem.rst
   kmeans_mr.rst
   kmeans_hadoop.rst
   kmeans_mahoot.rst
   mahout_tuto.rst
   sources.rst




