.. _kmeansmahoot:

***************************************************
Tools for Saclable Machine Learning Algorithm 
***************************************************
There are several machine-learning and statistics tools to deal with large (sometimes big) dataset. An non exhaustive list would include: Mahoot, ScaleR, MLBase,Cetas, Skytree, Ayasdi and VM.

Only a few of them are open source.  

Mahout
======

`Apache Mahout <http://mahout.apache.org/>`_ is certainly the most fertile option for large scale machine learning algorithm. Mahout uses Hadoop for severals algorithms. However, it remains independent of Hadoop since many machine learning algorithms does not fit the MapReduce paradigm. 

An exhaustive list of the algorithms already implemented in Mahout is available `here <https://cwiki.apache.org/confluence/display/MAHOUT/Algorithms>`_. As show in this list, there is a wide range of ML algorithms available in Mahout for classification, clustering and collaborative filtering. 

We note that the SVM and the EM algorithm are available and it appears the many people have developed a MapReduce version of the GMM algorithm for Mahout but it is not yet part of the library.

The advantages of Mahoot are:
	* Open source (Apache Licence)
	* Big and good documentation
	* Large community
	* Large scalability

The k-means problem with Mahout
-------------------------------
K_means algorithm is already implemented in Mahoot, so there is nothing to do. 
To be more exact there are several implementations available for this algorithm which are all described in chapter 9 of the book Mahout in Action, which can be downloaded `here <http://net.pku.edu.cn/~course/cs402/2012/book/%5BMahout.in.Action%282011%29%5D.Sean.Owen.pdf>`_.

To see how to run the k-means example with mahout refer to the :ref:`Mahout Tutorial <mahout_tuto>`.


The k-means clustering algorithm is run using either the KMeansClusterer or the KmeansDriver class. KmeansDriver does an in-memory clustering whereas KMeansclusterer launches k-means as a MapReduce job. Both methods can be executed on local disk or on a Hadoop cluster. 


An algorithm can be powerfully scalable even if the algorithm is nonparallel (it is the case for SVM). 


H20
===

`H20 <http://0xdata.github.com/h2o/>`_ is a new, open source, machine learning platform design to deal with large data set. Like Mahout it can use data stored in the Hadoop Distributed File System (HDFS).  
As this time there is only a few ML algorithm implemented. 


MLBase
======
`MLbase  <http://www.mlbase.org/>`_, like Spark and Sharl, is part of the Berckley Data Analytics Stack (BDAS). 
"MLbase provides functionality to end users for a wide variety of common machine learning tasks: classification, regression, collaborative filtering, and more general exploratory data analysis techniques such as dimensionality reduction, feature selection, and data visualization. Moreover, MLbase provides a natural platform for ML researchers to develop novel methods for these tasks."


LINKS
========== 
 
You can find valuable information on software libraries for large scale machine learning here: http://www.quora.com/What-are-some-software-libraries-for-large-scale-learning





