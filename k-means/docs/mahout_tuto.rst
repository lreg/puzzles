.. _mahout_tuto: 


*******************
How to use Mahout
********************

Installing Mahout 
==================

You need to have Hadoop installed!  

Download the sources at https://cwiki.apache.org/MAHOUT/buildingmahout.html (as far as I know there is no brew formulae for Mahout). 

The easiest solution is probably to follow the recommendation::

	svn co http://svn.apache.org/repos/asf/mahout/trunk

If you download the sources from one of the suggested mirrora make sure you pick one with *-src* extension. For instance choose mahout-distribution-0.7-src.zip rather than mahout-distribution-0.7.zip if you want to install it easily with Maven (only -src.zip contained the pom.xml file necessary for maven). Once the sources downloaded, move to the source directory and install them with maven::

	cd mahout-distribution-0.7-src
	mvm clean install  

Make sure you compile the core and the examples::

	cd core
	mvn compile 
	cd ../examples 
	mvn compile 


Test Mahout
============

First program with Mahout (command line)
-----------------------------------------

An easy way to run Mahout example is to use the mahout-core-*-job.jar. 
Lets take as example the Mahout Recommendation Engine. We use the movie lens dataset, you can download the data ML-100k `here <http://grouplens.org/node/73>`_.
 

First, start Hadoop::

	hadoop namenode -format 

Copy the data ml-100k/u.data onto HDFS, move to ml-100k and::

	hadoop fs -put u.data u.data 

You need to find where your mahout-core-*-job.jar is located. In my case mahout-core-0.8-SNAPSHOT-job.jar is located in $MAHOUT_HOME/core/target. Move to this directory an run the following command::

	hadoop jar mahout-core-*-job.jar org.apache.mahout.cf.taste.hadoop.item.RecommenderJob -s SIMILARITY_COOCCURRENCE --input u.data --output output

The -s option specifies the similarity measurement we want the recommender to use. 
With SIMILARITY_COOCCURRENCE, two items are similar if they often appear together in users ratings. 
  

Then Mahout computes the recommendations by running several Hadoop map reduce jobs. 
To see the result of the recommender, copy and merge the files from HDFS to your local file system type::

	hadoop fs -getmerge output output.txt  


Source: http://chimpler.wordpress.com/2013/02/20/playing-with-the-mahout-recommendation-engine-on-a-hadoop-cluster/



Same test with eclipse 
=======================

We suppose m2e plugging is installed. 

You can import the Mahout source in Eclipse: File -> Import -> Maven -> Existing Maven Project. 

Browse the local file system to find your $MAHOUT_HOME and import. (Check you have the Maven Dependencies) 

Find the source of the above example in the project explorer. In my case the source file is at : mahout-core/src/main/java under org.apache.mahout.cf.taste.hadoop.item. Select the source file RecommenderJob.java. 

To run it, right click on the file and chose Run As -> Run Configuration. 
You need to give the program arguments. To run it locally set up: 
	-s SIMILARITY_COOCCURRENCE
	--input /Users/lise/Downloads/ml-100k/u.data
	--output /Users/lise/output  

To run it on the HDFS, once you have put the u.data file into /user/lise/u.data set up the following arguments:
	-s SIMILARITY_COOCCURRENCE
	--input hdfs://localhost:8020/users/lise/u.data
	--output hdfs://localhost:8020/users/lise/output  




Running Mahout In Action (mia) examples:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





Ressource 
----------
If you have problems using Mahout, you may find some help on the mailing list http://mail-archives.apache.org/mod_mbox/mahout-user/.  