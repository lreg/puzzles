#!/usr/bin/env python

import hadoopy
import pickle 
from random import random
import csv 
import time
import pylab
import kmeans_mr_local 


input_path = 'data.csv'
script_path = 'kmeans_mr_local.py'
output_path = 'hadoopy-test-data/out/%f/' % time.time()

def parse_data(csv_file):
    '''
         Parses data of csv file into numpy 2d array 
    '''
    fid = open(csv_file)
   
    reader = csv.reader(fid)
    reader.next()
    for i,row in enumerate(reader):
        float_row = [float(data) for data in row]
        yield i, float_row
    fid.close()
    
    
def init_centroids(K=10):
    centroids = {}
    for cluster in range(K):
         centroids[cluster] = [random(), random()]  
    with open('clusters.pkl', 'w') as fid:
            pickle.dump(centroids, fid)
    fid.close()
   
   
def update_centroids(alist, cuttof = 1e-9):
    
    with open('clusters.pkl', 'r') as fid:
        old_centroids = pickle.load(fid)
        fid.close()
        
    maxdist = 0
    for (k,v) in alist:
       
        ox,oy = old_centroids[k]
        x,y = v
        dist = (x-ox)**2+(y-oy)**2
        old_centroids[k] = [x,y]
        if dist>maxdist:
              maxdist = dist
    
    #update the centroids values
    with open('clusters.pkl', 'w') as fid:
        pickle.dump(old_centroids, fid)
        fid.close()
    
    if maxdist<cuttof:
        return -1
    else:
        return 1
 
 
def plot_data(input_path, centroids_dict):
    K = max(centroids_dict.keys())
    for (id,data) in parse_data(input_path):
        for (cluster, x) in kmeans_mr_local.map(id, data):
            col = cluster*1.0/K
            pylab.plot(data[0],data[1], '.', color = (col,(col*2)%1,(col*3)%1))
    for (cluster, position) in centroids_dict.items():
        pylab.plot(position[0],position[1],'xr')
        
      
def main1():
    """
    Example without writting the output
    """
    init_centroids()
    
    while(True):
        output_centroids = hadoopy.launch_local(parse_data(input_path), None, script_path ) ['output']
        centroids_dict = dict(output_centroids)
        res = update_centroids(centroids_dict.items(), cuttof = 1e-9)
        
        if res==-1:
            print "FINISH"
            return centroids_dict
            
        
def main2():
    """
    Examples with output file 
    """
    init_centroids()
    i = 0
    while(True):
        # Note : you cannot rewrite a file  
        fileout = output_path+str(i)
        i += 1
        hadoopy.launch_local(parse_data(input_path), fileout , script_path )
        
        #The written file is a .tb 
        output_centroids = hadoopy.readtb(fileout)
        centroids_dict = dict(output_centroids)
        res = update_centroids(centroids_dict.items(), cuttof = 1e-9)
        
        if res==-1:
            print "FINISH"
            return centroids_dict 
        
        
if __name__ == "__main__":
    centroids_dict = main1()
    plot_data(input_path, centroids_dict)
    pylab.show() 
    
    
    
    