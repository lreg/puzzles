.. _readme_kmeans::


*******************************
K-means with Lloyd's Algorithm
*******************************


Problem
=======
Lloyd’s algorithm for k-means clusteringThis algorithm is the most widely used approach, and is often simply known as ‘the k-means algorithm’. Starting with an initial set of means, m1, ..., mk, the algorithm proceeds via two iterative steps:
1. Assignment: 
	Assign each vector to the cluster whose mean is nearest. (Assume Euclidian space, so you can use Pythagoras’ theorem to obtain distances in the obvious way.)2. Update: 
	Update each mean to be the centroid of its vectors (i.e. using Equation 1 above).These steps are repeated until the means have converged. You may choose any method to initialise the means, although it is reasonable to pick k data vectors at random (the Forgy method). It is up to you to select an appropriate test for convergence.


Task
====
Write a program which uses Lloyd’s algorithm to perform k-means clustering on these vectors.

Input
=====
A csv file containing 2-dimensional vectors represented as comma separated values with one vector per line.


Output
======

For each cluster your code should output the centroid, defined as the mean of all 2-d vectors within the cluster: :math:`m_i = \frac{1}{|Si|} \sum_{x_j in S_i} x_j`.


Notes
=====

To run the program with a given file 'data.csv' to find a maximum of N clusters run the following command::

	python kmeans.py data.csv N


The maximum number of cluster is not necessarily reached since the method can fall into a local minimum depending on the seeds.

The program prints out the centroids of the clusters found and plot the solution where each cluster has a single colour.


Dependencies
============
The program uses numpy to deal efficiently with large arrays and plots the data using pylab.  

