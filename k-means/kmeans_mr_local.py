#!/usr/bin/env python

import hadoopy
import pickle
from random import random


    
    
def load_centroids():
    """centroids are stored in a file"""
    with open('/Users/lise/Documents/Prog/python/puzzles/k-means/clusters.pkl') as fid:
        centroids = pickle.load(fid)
        fid.close()
        return centroids
    
def map(key,value):
    """ Receives (data index, points_coordinates)
        Returns (cluster_num, [points_coordinate,count]) 
    """
    current_centroids = load_centroids()
    (x,y) = value
    dist = [(x-kx)**2+(y-ky)**2  for (kx,ky) in current_centroids.values()]
    arg_min = sorted(range(len(dist)), key = dist.__getitem__)[0]
    cluster_num = current_centroids.keys()[arg_min]
    yield cluster_num, (x,y)
    
    


def reduce(key,values):
    """
    Receives (cluster_num, list of [points_coordinate,count]) as key, value
    Returns (cluster_num, new_centroids coordinates) 
    """
    x,y = 0,0
    
    for cpt, (px,py) in enumerate(values):
        x += px
        y += py
       
    #compute centroids
    x /=cpt
    y /=cpt

    yield key, (x,y)
    


    
        
if __name__ == "__main__":
    hadoopy.run(map,reduce,doc=__doc__)

    
        