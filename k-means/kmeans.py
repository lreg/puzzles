import csv
import sys
import numpy as np
import pylab
from random import random

def kmeans_lloyd(data, nb_cluster, cutoff = 1e-9 ):
    """
    Clusters the 2d points of data into maximum number of clusters given by nb_cluster
    using the lloyd algorithm for k means. 
    At each step of the iteration, each data point is assigned to its closes cluster.
    The centroids of the clusters are then update. 
    The new centroid is given by the mean of all points within the cluster.
    The iterative algorithm stops when the distance between
    the centroid and the updated centroids is below cutoff
    
    Returns: 
        - the centroids of the final clusters
        - the membership of each data point to the final clusters
    """
    
    N = len(data)
    
    # init means to random values
    means = np.array([[random(), random()] for _ in range(nb_cluster)])
    
    while(True):
        
        #step 1: membership of each data point
        cluster_k = np.zeros(N, int)
        for i,(x,y) in enumerate(data):
            dist = [(x-kx)**2 + (y-ky)**2 for (kx, ky) in means]
            cluster_k[i] = np.argmin(np.array(dist)) 
        
        #step 2: re-evaluate clusters centroids
        newmeans = np.zeros((nb_cluster,2))
        diff_means = np.zeros(nb_cluster)
        for k in range(nb_cluster):
            pos = np.where(cluster_k == k)[0]
            if len(pos):
                tmpdata = data[pos] 
                tmpmean = np.mean(np.array(tmpdata),0)
                diff_means[k] = (tmpmean[0] - means[k][0])**2+
                                (tmpmean[1] - means[k][1])**2
                newmeans[k]  = tmpmean
            else:
                newmeans[k] = means[k]
                diff_means[k] = 0
        means = newmeans
    
        if max(diff_means)<cutoff:
            break
        
    return means, cluster_k  
    

def plot_data(data, cluster_k, max_cluster):
    for i, (x,y) in enumerate(data):
        col = cluster_k[i]*1.0/max_cluster
        pylab.plot(x,y, '.', color = (col,(col*2)%1,(col*3)%1))
    pylab.show()


def parse_data(csv_file):
    '''
         Parses data of csv file into numpy 2d array 
    '''
    fid = open(csv_file)
    rowdata = []
    reader = csv.reader(fid)
    reader.next()
    for row in reader:
        float_row = [float(data) for data in row]
        rowdata.append(float_row) 
    fid.close()
    return np.array(rowdata)




if __name__ == '__main__':
    print len(sys.argv)
    if len(sys.argv)<3:
        sys.stderr.write("Usage: " + sys.argv[0] + ' filename ' + ' max_cluster ' + '\n')
    else:
        
        filename = sys.argv[1]
        max_cluster = int(sys.argv[2])
        
        data = parse_data(filename)
        centroid_clusters, cluster_k  = kmeans_lloyd(data, max_cluster)
        distinct_clusters = set(cluster_k)
 
        for i,centroid in enumerate(centroid_clusters):
            if i in distinct_clusters: 
                print centroid

        plot_data(data,cluster_k,max_cluster)
     
    