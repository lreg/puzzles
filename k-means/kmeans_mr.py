#!/usr/bin/env python

import hadoopy
import pickle
from random import random

class Mapper(object):   
    def __init__(self):
            filepath = "hadoopy-kmeans/latest.tb"
            self._current_centroids =  dict(hadoopy.readtb(filepath))
    
    
    def map(self,key,value):
        """ Receives (data index, points_coordinates)
            Returns (cluster_num, [points_coordinate,count]) 
        """
        (x,y) = value
        dist = [(x-kx)**2+(y-ky)**2  for (kx,ky) in self._current_centroids.values()]
        arg_min= sorted(range(len(dist)), key = dist.__getitem__)[0]
        cluster_num = self._current_centroids.keys()[arg_min]
        yield cluster_num, (x,y)
    

class Reducer(object):
    def __init__(self):
        pass
    
    def reduce(self,key,values):
        """
        Receives (cluster_num, list of [points_coordinate,count]) as key, value
        Returns (cluster_num, new_centroids coordinates) 
        """
        x, y = 0,0
        #cumulative sum
        for cpt, (px,py) in enumerate(values):
            x += px
            y += py
        #compute centroids
        x /=cpt
        y /=cpt
        yield key, (x,y)
    
       
if __name__ == "__main__":
    hadoopy.run(Mapper,Reducer)

    
        