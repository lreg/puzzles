"""
Implementation of the k-means algorithm using the 
MapReduce paradigm 
"""
import sys
import csv
import pylab
import multiprocessing
from random import random
from itertools import repeat

_plot_opt = 1


def kmeans_mapreduce(data,max_cluster, cutoff = 1e-9):

    #init centroids
    centroids = {}
    for i in range(max_cluster):
        centroids[i] = [random(), random()]
        
        
    while(True):
        if _plot_opt == 1:
            pylab.figure(1)
            
        #map step
        partial_sum = pool.map(Map,zip(data, repeat(centroids.items())))

        #collapse data
        global_sum = Partition(partial_sum)
        
        #reduce step
        update_centroids = pool.map(Reduce, global_sum.items())
        
        #convergence condition
        maxdist_centroids = 0
        update_center = []
        for [i,(x, y)] in update_centroids:
            [kx, ky] = centroids[i]
            dist = (x-kx)**2 + (y-ky)**2
            if dist> maxdist_centroids:
                maxdist_centroids = dist
            centroids[i] = [x,y]
            update_center.append(i)
       
        
        #stop iteration: print centroids of each cluster
        if maxdist_centroids<cutoff:
            #delete clusters with no points
            for key in centroids.keys():
                if key not in update_center:
                    del centroids[key] 
            for i, (x,y) in centroids.items():
                if i in global_sum.keys():
                    print (x,y)
                   
            return centroids
        
         
def Map((alist, centroids)):
    """
    Given a list of 2d points and the current cluster position,
    assign each points to the right cluster 
    returns the (sum, occ) for each cluster 
    """
    
    partial_sum = [[[0,0],0] for _ in range(len(centroids))]
    
    for x,y in alist:
        dist = [(x-kx)**2 + (y-ky)**2 for (_,[kx, ky]) in centroids]
        #argmin of dist 
        argmin = sorted(range(len(dist)), key = dist.__getitem__)[0]
        partial_sum[argmin][0][0] +=x
        partial_sum[argmin][0][1] +=y
        partial_sum[argmin][1] += 1
    
    return partial_sum 

def Partition(alist):
    global_sum = {}
    
    # aggregation of partial sums into a dict
    for sublist in alist:
        for i, val in enumerate(sublist):
            if not val[1] == 0: 
                # Append the tuple to the list in the map
                try:
                    global_sum[i].append(val)
                except KeyError:
                    global_sum[i] = [val]
               
    return global_sum
    
 
def Reduce(alist):
    """
    Aggregates the partial sums and compute the new centroids position
    """

    nx, ny, nnb = 0, 0, 0 
    key, val = alist
    
    for [[x,y], nb] in val:
            nx += x
            ny += y
            nnb += nb
    return (key, [nx/nnb, ny/nnb])
    

def chunks(alist, n):
    """
    A generator function for chopping up a given list into chunks of
    length n.
    """
    for i in xrange(0, len(alist), n):
        yield alist[i:i+n]
    
def parse_data(csv_file):
    '''
         Parses data of csv file into numpy 2d array 
    '''
    fid = open(csv_file)
    rowdata = []
    reader = csv.reader(fid)
    reader.next()
    for row in reader:
        float_row = [float(data) for data in row]
        rowdata.append(float_row) 
    fid.close()
    
    return rowdata


def plot_data(data, centroids):
    for (x,y) in data:
        dist = [(x-kx)**2 + (y-ky)**2 for (_,[kx, ky]) in centroids.items()]
        #argmin of dist 
        cluster = sorted(range(len(dist)), key = dist.__getitem__)[0]
        col = cluster*1.0/len(centroids)
        pylab.plot(x,y, '.', color = (col,(col*2)%1,(col*3)%1))
    for (x,y) in centroids.values():
        pylab.plot(x,y, 'rx')
                  
    pylab.show()    
        

if __name__ == '__main__':
    if len(sys.argv)<2:
        sys.stderr.write("Usage: " + sys.argv[0] + 'filename' + 'max_cluster')
    else:
        
        filename = sys.argv[1]
        max_cluster = int(sys.argv[2])
        
#        global centroids
        
        # Build a pool of 10 processes
        pool = multiprocessing.Pool(processes=10,)
        
        # Fragment the raw data into 10 chunks
        data = parse_data(filename)
        partitioned_data = list(chunks(data, len(data) / 10))
        print len(data), len(partitioned_data)
        

        centroids = kmeans_mapreduce(partitioned_data, max_cluster)
        plot_data(data, centroids)