'''
    Spotify Puzzle - Reversed Binary Numbers
    https://www.spotify.com/fr/jobs/tech/reversed-binary/
    Program for reversing numbers in binary. 
    For instance, the binary representation of 13 is 1101, 
    and reversing it gives 1011, which corresponds to number 11.
    
    L.Regnier lise.regnier@gmail.com
'''

import re
import doctest

def int_to_bin(n):
    """
        Converts an int n to 
        its binary representation (as a string)
        Note: Equivalent to  bin(n)[2:]
    """
    
    bin_l = []
    while n > 0:
        bin_l.insert(0, str(int(n % 2)))
        n = n // 2
    
    bin_str = "".join(bin_l)

    return bin_str

    
def reversed_binary(n):
    """
        Returns the int corresponding 
        to the reversed binary
        >>> reversed_binary(13)
        11
        >>> reversed_binary(47)
        61
    """
    
    bin_str = int_to_bin(n)
    
    #bin to int with bin in reversed order (from left to right)
    res = 0
    for i, n in enumerate(bin_str):
        res += 2 ** i * int(n)
    return res

def format_input(n_str):
    """
    Checks if the input corresponds to
    an integer n such as: 1<=n<=1e9
    """
    
    if not re.match("^[0-9]*$", n_str)  :
        raise ValueError("Input must be an integer")
    
    n = int(n_str)
    
    if n < 1 or n > 1e9:
        raise ValueError("Input n must be an integer such as 1<=n<=1e9")
    else:
        return n

if __name__ == "__main__":
    
    doctest.testmod()
    
    n_str = raw_input()
    n = format_input(n_str)
    res = reversed_binary(n)
    print res
    
