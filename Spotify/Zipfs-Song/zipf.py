'''
    Spotify Puzzle - Zipfs law
    Zipf's Law is an empirical law originally formulated about word frequencies in natural languages,
    but it has been observed that many natural phenomena, such as population sizes and incomes,
    approximately follow the same law. 
    It predicts that the relative frequency of the i'th most common object (in this case, a song)
    should be proportional to 1/i. 
    
    
    Note:
    - This programs fails the spotify test when calling check_input
    I suppose, one of their test does not follow the given input condition
    
    
    L.Regnier lise.regnier@gmail.com
'''


import sys
import re

    
def zipf(n, m, S, F):
    """
        Returns the m best song according to zipfs law where :
        n : number of track              (1<= n <= 50000)
        m : number of track to select        (1<= m <= n)
        S : array of track_names       (max 30 char long)
        F : array of scores     (each score 0<=fi<=10e12)
    """
    
    Factori = range(1, n + 1) 
    #Computes the scores according to the given law (reverse order)
    Feq = [-1 * fi * facti for (fi, facti) in zip(F, Factori)]
    
    #Finds index of the best m higest scores
    pos = argsort(Feq)[0:m]
    
    #Get the corresponding songs name
    res = [S[i] for i in pos ]
    return res  


def argsort(seq):
    return sorted(range(len(seq)), key=seq.__getitem__)



def check_input(n, m, S, F):
    """
        Checks if the input parameters lies in the right intervals
    """
    for var, min_, max_ in [(n, 0, 5000), (m, 1, n)]:
        if var < min_ or var > max_:
            raise ValueError(" 1<= n <= 50000 , 1<= m <= n")

    for si in S:
        if not re.match("^[a-z0-9_]*$", si) or len(si) > 30 :
            raise ValueError("Track names must contain 'a'-'z', '0'-'9', and underscore ('_') only")

    for fi in F:
        if fi < 0 or fi > 10e12:
            raise ValueError("0<=fi<=10e12")



        
if __name__ == "__main__":
    
    [nb_song, nb_sel] = [int(input_str) for input_str in sys.stdin.readline().split(" ")]
    
    F = []  
    S = []  
    for i in range(nb_song):
        fi, si = sys.stdin.readline().split(" ")
        F.append(int(fi))
        S.append(si)
    
    #check_input(nb_song, nb_sel, S, F)
    
    res = zipf(nb_song, nb_sel, S, F)
    for track_name in res:
        if track_name.endswith('\n'):
            print track_name,
        else:
            print track_name

