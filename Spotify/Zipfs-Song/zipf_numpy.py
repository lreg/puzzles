'''
    Spotify Puzzle - Zipfs law
    Zipf's Law is an empirical law originally formulated about word frequencies in natural languages,
    but it has been observed that many natural phenomena, such as population sizes and incomes,
    approximately follow the same law.
    It predicts that the relative frequency of the i'th most common object (in this case, a song)
    should be proportional to 1/i.
    
    
    Note:
    - This scripts uses numpy which is the most efficient way in python to deal with large array
    
    
    L.Regnier lise.regnier@gmail.com
'''


import sys
import re
import numpy as np

def zipf(n, m, S, F):
    """
        Returns the m best song according to zipfs law where :
        n : number of track              (1<= n <= 50000)
        m : number of track to select        (1<= m <= n)
        S : array of track_names       (max 30 char long)
        F : array of scores     (each score 0<=fi<=10e12)
    """
    
    Factori = np.arange(1, n + 1)*-1
    Feq =  F*Factori
    pos = np.argsort(Feq)[0:m]
    
    return S[pos]




if __name__ == "__main__":
    
    [nb_song, nb_sel] = [int(input_str) for input_str in sys.stdin.readline().split(" ")]
    
    F = np.zeros(nb_song, int)
    S = np.zeros(nb_song, 'S30')
    for i in range(nb_song):
        fi, si = sys.stdin.readline().split(" ")
        F[i] = int(fi)
        S[i] = si
    
    res = zipf(nb_song, nb_sel, S, F)
    for track_name in res:
        if track_name.endswith('\n'):
            print track_name,
        else:
            print track_name



