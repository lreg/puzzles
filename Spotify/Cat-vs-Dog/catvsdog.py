# !/usr/bin/env python
# encoding: utf-8

'''
    Spotify Puzzle - Cat versus Dog
    https://www.spotify.com/fr/jobs/tech/catvsdog/
    
    To cat vs dog problem can be consider a Clique problem where, 
    the votes are the vertex of a graph and 2 vertices
    are connected if they do not cancel each other. 
    
    The list of maximal cliques is obtained using 
    Bron-Kerbosh algorithm. 
    
    Finally, the maximal number of people that can be satisfied
    is given by the length of the maximum clique.
    
    L.Regnier lise.regnier@gmail.com
'''



def build_graph(votes):
    ''' 
        builds graph with adjacent lists (dictionary)
        vertex (key) : vote
        edges (values): compatible votes
    '''
    G = {}
    for i, vote1 in enumerate(votes):
        #create node
        G[i] = []
        for j, vote2 in enumerate(votes):
            if (i != j) and not(canceled(vote1, vote2)):
                #add edge
                G[i].append(j)        
    return G 



def canceled(vote1, vote2):
    '''
        returns true if one vote cancel the other vote
        i.e. the pet loved in vote1 is hated in vote2 (and vice-versa)
    '''
    return  (vote1[0] == vote2[1]) or (vote1[1] == vote2[0])



def bron_kerbosh(R, P, X, G):
    '''
        iterative bron_kerbosh algorithm (without pivot)
        Returns the maximum clique
    '''
    if not P and not X:
        return R  #maximum clique

    maxclique = set()
    for v in  P.copy(): 
        P.remove(v)
        R_v = R.union([v])
        P_v = P.intersection(G[v])
        X_v = X.intersection(G[v])
        r = bron_kerbosh(R_v, P_v, X_v, G)
        if len(r) > len(maxclique):
            maxclique = r
        X.add(v)
    return maxclique

    


if __name__ == "__main__":
    
    nb_cases = int(raw_input())
    all_res = []
    
    for _ in range(nb_cases):
        nb_cats, nb_dogs, nb_voters = [int(num) for num in raw_input().split(" ")]
        
        votes = []
        for _ in range(nb_voters):
            love, hate = [vote.strip() for vote in raw_input().split(" ")]
            votes.append([love, hate])

        G = build_graph(votes)

        maximum_clique = bron_kerbosh(set(), set(G.keys()), set(), G)
        all_res.append(len(maximum_clique));

    for res in all_res:
        print res
        
    
