# !/usr/bin/env python
# encoding: utf-8

'''
    Spotify Puzzle - Cat versus Dog
    https://www.spotify.com/fr/jobs/tech/catvsdog/
    
    To cat vs dog problem can be consider a Clique problem where, 
    the votes are the vertex of a graph and 2 vertices
    are connected if they do not cancel each other. 
    
    The list of maximal cliques is obtained using 
    Bron-Kerbosh algorithm. 
    
    Finally, the maximal number of people that can be satisfied
    is given by the length of the maximum clique.
    
    L.Regnier lise.regnier@gmail.com
'''
import networkx as nx

def build_graph(votes):
    ''' 
        build graph with adjacent lists (dictionary)
        vertex (key) : vote
        edges (values): compatible votes
    '''
    G = nx.Graph()
    for i, vote1 in enumerate(votes):
        #create node
        G.add_node(i)
        for j, vote2 in enumerate(votes):
            if (i != j) and not(canceled(vote1, vote2)):
                #add edge
                G.add_edge(i, j)        
    return G 

def canceled(vote1, vote2):
    '''
        returns true if one vote cancel the other vote
        i.e. the pet loved by vote1 is hated in vote2 (and vice-versa)
    '''
    return  (vote1[0] == vote2[1]) or (vote1[1] == vote2[0])


if __name__ == "__main__":
    
    nb_cases = int(raw_input())
    all_res = []
    
    for _ in range(nb_cases):
        nb_cats, nb_dogs, nb_voters = [int(num) for num in raw_input().split(" ")]
        
        votes = []
        for _ in range(nb_voters):
            love, hate = [vote.strip() for vote in raw_input().split(" ")]
            votes.append([love, hate])

        G = build_graph(votes)

        maximal_cliques = list(nx.find_cliques(G))
        maximum_clique = max(maximal_cliques, key=len)
        all_res.append(len(maximum_clique));

    for res in all_res:
        print res
